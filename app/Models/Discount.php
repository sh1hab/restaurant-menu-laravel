<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Discount extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'amount',
        'type',
    ];

    public function closestDiscount($model, $relation, $fallback){
        return $this->$relation ? $model->$relation->$fallback : $fallback;  
    }
    
     /**
     * Get the parent commentable model (category or item)
     */
    public function commentable(): MorphTo
    {
        return $this->morphTo();
    }
}
