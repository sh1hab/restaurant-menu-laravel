<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'parent_id'];


    public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_id');
    }

    /*
        * Get all of the category's children.
        */

    public function subCategory()
    {
        return $this->hasMany(Category::class, 'parent_id');
    }

    /**
     * Get all of the category's items.
     */
    public function items()
    {
        return $this->hasMany(Item::class);
    }

    /**
     * Get all of the items discounts.
     */
    public function discounts()
    {
        return $this->morphMany(Discount::class, 'discountable');
    }

    public function closestDiscount($model, $relation, $fallback){
        return $this->discounts ? $this->discounts : $this->parent->discounts;  
    }
}
