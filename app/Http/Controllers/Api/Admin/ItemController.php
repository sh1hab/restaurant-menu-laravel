<?php

namespace App\Http\Controllers\Api\Admin;

use Illuminate\Http\Request;
use App\Responders\Traits\HasResponseStructure;
use App\Http\Requests\ItemCreateRequest;
use App\Models\Item;
use App\Http\Controllers\Controller;


class ItemController extends Controller
{
    use HasResponseStructure;
    
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $categories = Item::with(['category', 'discounts'])->paginate($request->get('limit', 100));

        return $this->getSuccessBag($categories, 200);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(ItemCreateRequest $request)
    {
        $category = Item::create($request->validated());

        return $this->getSuccessBag($category, 201);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
