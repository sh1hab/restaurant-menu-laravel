<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Category;
use App\Models\Item;
use App\Models\Discount;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        \App\Models\User::factory()->create([
            'name' => 'admin',
            'email' => 'admin@example.com',
        ]);

        Category::factory(5)->create(
            [
                'parent_id' => Category::factory(1),
            ]
        )->each(function ($category) {
            $category->items()->saveMany(Item::factory(1)->make());
            Discount::factory(1)->create([
                'discountable_type' => Category::class,
                'discountable_id' => $category->id,
            ]);
        });

        // Discount::factory(5)->create([
        //     'discountable_type' => Category::class,
        // ]);

        Discount::factory(5)->create([
            'discountable_type' => Item::class,
        ]);

        Discount::factory(1)->create([
            'all' => 1,
        ]);
    }
}
