<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Category>
 */
class CategoryFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $foodTypes = ['Pizza', 'Pasta', 'Salad', 'Dessert', 'Drinks'];
        
        return [
            'name' => $foodTypes[ array_rand($foodTypes) ],
            'parent_id' => null,
        ];
    }
}
