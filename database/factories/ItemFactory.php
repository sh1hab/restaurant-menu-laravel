<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Category;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Item>
 */
class ItemFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $foods = ['swedeish meatballs', 'lasagna', 'caesar salad', 'tiramisu', 'coke'];

        return [
            'name' => $foods[ array_rand($foods) ],
            'category_id' => Category::factory(),
        ];
    }
}
