<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Models\User;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;
use App\Responders\Traits\HasResponseStructure;

class AuthController extends Controller
{
    use HasResponseStructure;
    
    protected string $grantType = 'authToken';

    public function __construct()
    {
        $this->middleware(['auth:sanctum'])->only('logout');
    }

    /**
     * @lrd:start
     * *User | admin login*
     *
     *
     *  Will work for both end users and admins
     * @lrd:end
     */

    public function login(LoginRequest $request)
    {
        if (!auth()->attempt($request->validated())) {
            abort(403, 'Invalid credentials');
        }

        $user = User::where('email', $request->email)->firstOrFail();
        return $this->authTokenResponse($user);
    }

    /**
     * @lrd:start
     * *Register end user*
     *
     * this api is used to register end users or clients | students
     * @lrd:end
     */

    public function register(RegisterRequest $request)
    {
        $user = User::create(
            array_merge(
                $request->safe()->except(['password']),
                [
                    'password' => Hash::make($request->password)
                ]
            )
        );

        $this->addRole($user);

        return $this->authTokenResponse($user);
    }

    /**
     * @lrd:start
     * *logout user*
     *
     * `Requires authentication`
     *
     * this api is used will work with both end users and admins.
     * @lrd:end
     */

    public function logout()
    {
        $user = auth()->user();

        if (is_null($user)) {
            $error = 'User is not authorized to perform this action.';
            throw new HttpResponseException($this->getExceptionMessage($error, JsonResponse::HTTP_UNAUTHORIZED));
        }

        $user->tokens()->delete();

        return $this->getSuccessBag('logout success', JsonResponse::HTTP_OK);

    }

    protected function addRole($user)
    {
        $user->assignRole(
            Role::where('name', $this->studentRole)->firstOrFail()
        );
    }

    protected function authTokenResponse($user)
    {
        $token = $user->createToken($this->grantType)->plainTextToken;
        return $this->getTokenResponse($this->grantType, $token);
    }
}
