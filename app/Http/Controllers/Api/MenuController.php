<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Responders\Traits\HasResponseStructure;
use App\Http\Controllers\Controller;
use App\Actions\GetMenu;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;


class MenuController extends Controller
{
    use HasResponseStructure, AuthorizesRequests, ValidatesRequests;

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request, GetMenu $getMenu)
    {
        $menu = $getMenu->handle($request);

        return $this->getSuccessBag($menu, 200);
    }
}
