<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'price',
        'category_id',
    ];

    /**
     * Get all of the items discounts.
     */
    public function discounts()
    {
        return $this->morphMany(Discount::class, 'discountable');
    }
}
