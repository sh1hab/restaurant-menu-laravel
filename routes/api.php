<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'admin', ], function () {
    Route::apiResource('categories', 'App\Http\Controllers\Api\Admin\CategoryController');
    Route::apiResource('discounts', 'App\Http\Controllers\Api\Admin\DiscountsController');
    Route::apiResource('items', 'App\Http\Controllers\Api\Admin\ItemController');
});

Route::group(['prefix' => 'auth', ], function () {
    Route::post('login', 'App\Http\Controllers\Api\Auth\AuthController@login');
});

Route::group(['prefix' => 'user', ], function () {
    Route::get('menu', 'App\Http\Controllers\Api\MenuController@index');
});
