<?php

namespace App\Actions;

use App\Models\Category;
use Illuminate\Http\Request;

class GetMenu
{
    public function handle(Request $request)
    {
        $categories = Category::with(['discounts:id,amount,discountable_type,discountable_id', 'items:id,name,category_id', 'items.discounts:id,amount,discountable_type,discountable_id', 'subCategory.items'])
        ->whereNull('parent_id')->get();

        return $categories;
    }
}
